all: truelisp

HY = hy
HYC = hyc3
HY2PY = hy2py3

FIBO = 15

# Compile
truelisp.py: truelisp.hy
	$(info Compiling (hy2py) $<)
	@$(HY2PY) $< > $@

truelisp: truelisp.hy
	@$(HYC) $<

# Tests
fibo: example-fibo.hy
	$(info Running $< $(FIBO))
	@$(HY) example-fibo.hy $(FIBO)

test: test.hy
	$(info Running $<)
	@$(HY) $<

check: test

# Clean
.PHONY: clean check test fibo truelisp
clean:
	@rm -vf *.py
	@rm -vrf __pycache__/
