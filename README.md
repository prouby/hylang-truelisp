Hylang Truelisp
===============

![master](https://framagit.org/prouby/hylang-truelisp/badges/master/pipeline.svg)

**Hylang implement of classic lisp, like car / cdr / list? / etc.**


## Features

| Name        | Description                           | Type     |
|-------------|---------------------------------------|----------|
| λ           | Define lambda function                | Macro    |
| lambda      | Define lambda function                | Macro    |
| car!        | Return first element of list          | Marco    |
| cdr!        | Return rest of list                   | Macro    |
| car         | Return first element of list          | Function |
| cdr         | Return rest of list                   | Function |
| append      | Concat 2 list                         | Function |
| list?       | True if arguement is an list          | Function |
| rmap        | Recursive impelmentation of map       | Function |
| map-list    | map l with function f                 | Function |
| reverse     | Reverse list                          | Function |
| remove-last | Remove last element of list           | Function |
| cons*       | cons multiple element with list       | Macro    |
| setl        | Define variable from list [key value] | Macro    |
| let1        | Define one local variable             | Macro    |
| let         | Define locals variables               | Macro    |


## Examples

### List
```hy
(import [truelisp [*]])

(setv lst [1 2 3 4 5])
(print (car lst))
(print (cdr lst))

(setv nlst (map-list (λ [x] (+ x 1)) lst))
(print nlst)
(print (reverse nlst))
```

### Let
```hy
(require [truelisp [*]])

(let [[foo 4]
      [bar 10]]
  (+ 2 (* foo bar)))
```
