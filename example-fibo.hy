;; -*- encoding: utf-8 -*-
(import  [truelisp [*]])
(require [truelisp [*]])

(defn usage [args]
  (print "Usage:" (car args) "<num>")
  (quit))

(defn fibo [n]
  (defn fibo_aux [u0 u1 n]
    (cond
      [(<= n 0) u0]
      [True (fibo_aux u1 (+ u0 u1) (- n 1))]))

  (fibo_aux 0 1 n))

(defmain [&rest args]
  (when (< (len args) 2) (usage args))
  (let1 [f (λ [x]
              (do
                (print "Calcul fibonacci of" x)
                (print (fibo (int x)))))]
        (map-list f (cdr args))))
