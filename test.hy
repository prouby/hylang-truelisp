;; test.hy --- Hylang lisp functions test.   -*- encoding: utf-8 -*-
;; Copyright © 2018 by Pierre-Antoine Rouby <contact@parouby.fr>
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;; Import functions
(import [truelisp [*]])

;; Import macros
(require [truelisp [*]])

;;;
;;; Function.
;;;

(defn test [name eq]
  (if eq
      (print (+ "  PASSED  --  " name))
      (assert eq (+ "  FAILED  --  " name))))

;;;
;;; Car.
;;;

(test "(car! [1 2 3 4])" (= (car! [1 2 3 4]) 1))
(test "(car  [1 2 3 4])" (= (car [1 2 3 4]) 1))

;;;
;;; Cdr.
;;;

(test "(cdr! [1 2 3 4])" (= (cdr! [1 2 3 4]) [2, 3, 4]))
(test "(cdr  [1 2 3 4])" (= (cdr [1 2 3 4]) [2, 3, 4]))

;;;
;;; List?
;;;

(test "(list? [1 2 3])" (list? [1 2 3]))
(test "(not (list? '(1 2 3))" (not (list? '(1 2 3))))

;;;
;;; Lambda.
;;;

(setv f (lambda [] (+ 1 1)))
(test "(lambda [] (+ 1 1))" (= 2 (f)))
(setv f (λ [] (+ 1 1)))
(test "(λ [] (+ 1 1))" (= 2 (f)))
(setv f (λ [x] (* x x)))
(test "(λ [x] (* x x))" (= 4 (f 2)))
(setv f (λ [x y] (* x y)))
(test "(λ [x y] (* x y))" (= 8 (f 2 4)))


;;;
;;; Rmap.
;;;

(test "(rmap f [1 2 3 4])"
      (= [2 3 4 5] (rmap (λ [x] (+ x 1)) [1 2 3 4])))

;;;
;;; Map-list.
;;;

(test "(map-list f [1 2 3 4])"
      (= [2 3 4 5] (map-list (λ [x] (+ x 1)) [1 2 3 4])))
(test "(map-list f ['a' 'b' 'c' 'd'])"
      (= [False False True False] (map-list (λ [x] (= x "c"))
                             ["a" "b" "c" "d"])))
;;;
;;; Reverse.
;;;

(test "(reverse [1 2 3])"
      (= [3 2 1] (reverse [1 2 3])))

;;;
;;; Cons*.
;;;

(test "(cons* 1 2 3 [4 5])"
      (= [1 2 3 4 5] (cons* 1 2 3 [4 5])))

;;;
;;; Setl.
;;;

(setl [setlvar 42])
(test "(setl [setlvar 42])"
      (= 42 setlvar))

;;;
;;; Let.
;;;

(test "(let1 [foo 42] foo)"
      (= 42 (let1 [foo 42] foo)))

(test "(let1 [foo 22] (let [bar 20] (+ foo bar)))"
      (= 42 (let1 [foo 22]
                 (let1 [bar 20]
                      (+ foo bar)))))

(test "(let [[var1 foo] [var2 bar]] (+ var1 var2)"
      (= "foobar" (let [[var1 "foo"] [var2 "bar"]]
                 (+ var1 var2))))

(test "(let ... (ax^2 + bx + c))"
      (= 98 (let [[a 5]
                  [b 2]
                  [c 10]
                  [x 4]]
                 (+ (* a (* x x))
                    (* b x)
                    c))))
