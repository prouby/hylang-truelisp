;;; truelisp.hy --- Hylang lisp functions.   -*- encoding: utf-8 -*-
;;; Copyright © 2018 by Pierre-Antoine Rouby <contact@parouby.fr>
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;;
;;; Lambda.
;;;

(defmacro λ [args expr] `(fn ~args ~expr))
(defmacro lambda [args expr] `(fn ~args ~expr))

;;;
;;; List.
;;;

(defmacro car! [l] `(get ~l 0))
(defmacro cdr! [l] `(cut ~l 1))

(defn car [l]
  (get l 0))

(defn cdr [l]
  (cut l 1))

(defn append [l1 l2]
  (+ l1 l2))

(defn list? [l]
  (= list (type l)))

;;;
;;; List avanced functions.
;;;

(defn rmap [f l]
  (if (empty? l) []
      (cons (f (car! l)) (rmap f (cdr! l)))))

(defn map-list [f l]
  (list (map f l)))

(defn reverse [l]
  (defn reverse_aux [l acc]
    (if (empty? l)
        acc
        (reverse_aux (cdr! l) (cons (car! l) acc))))
  (reverse_aux l []))

(defn remove-last [l]
  (if (empty? (cdr! l))
      []
      (cons (car! l) (remove-last (cdr! l)))))

(defmacro cons* [&rest args]
  `(+ (remove-last ~args) (last ~args)))

;;;
;;; Let.
;;;

(defmacro setl [[var value]]
  `(setv ~var ~value))

(defmacro let1 [var body]
  `(do
     (setl ~var)
     ~body))

(defmacro let [vars body]
  (if (empty? vars)
      `~body
      `(let1 ~(car! vars)
             (let ~(cdr! vars)
                  ~body))))
